

// Set up for selecting random images and placing it in the background
var images = ["avacado_image.jpg", "pepper_image.jpg", "tomato_image.jpg"];
var randomImage =  images[Math.floor(Math.random() * images.length)];

$('.image_header').css({'background': 'url("images/' + randomImage + '") no-repeat', "background-size": "cover", "background-position": "center"});


// Remove emptyList message if list has children.
function emptyList() {	
	if ($(".list").children().length <= 0) {
		$("#emptyListMessage").show();
	} else {
		$("#emptyListMessage").hide();
	}
}

// The beginning intro animation.
function beginIntro() {
	var $introTitle = $("#introTitle");
	var $wrapper = $("#wrapper");
	$introTitle.hide();
	$wrapper.hide();
	$introTitle.fadeToggle(1500, function() {
		$introTitle.fadeToggle(1000, function(){
			$("#overlay").hide();
			$wrapper.fadeToggle(500);
		});
	});
}

// Variable for missing title element selector.
var $enterMessage = $(".enter_message");

// Variables to create list item element.
var minusSign = '<li class="minus"><button class="icon-minus"></button></li>';
var markSign = '<li class="mark"><button class="icon-checkmark"></button></li>';
var removeSign = '<li class="remove"><button class="icon-cancel"></button></li>';

// Click event to apply input fiels to list item.			
$(".add_item_box").on("click", "#add_item", function() {

// Variables to retrieve values in input fields.
var $item_title = $(this).closest(".add_item_box").find("#item_name");
var $item_desc = $(this).closest(".add_item_box").find("#item_desc");
	
	// Check if "Add item" field is not empty.	
	if ($item_title.val().trim() !== "") {

		// Create elements of list items to apply input values
		var listItemTitle = '<li class="list_item_title freight_sans"><p>' + $item_title.val() +'</p></li>';
		var listItemDesc = '<li class="list_item_desc freight_text"><p>' + $item_desc.val() +'</p></li>';

		// Create element that allows us to put all the list elements together.
		var ListItem = '<ul class="list_item clear">' + listItemTitle + listItemDesc + minusSign + markSign + removeSign +'</ul>';

		// Add the list item to the list.
		$(".list").append(ListItem);
		
		// Reset input values
		$item_title.val("");
 		$item_desc.val("");

 		emptyList();
 		$enterMessage.fadeOut("fast");
	} else {
		$enterMessage.fadeIn("fast");
	} 
});


$(document).keyup(function(event) {

// Variables to retrieve values in input fields.
var $item_title = $(".add_item_box").find($("#item_name"));
var $item_desc = $(".add_item_box").find($("#item_desc"));

	// If enter key is pressed apply input values to list.
	if (event.keyCode === 13) {
		if ( $("#item_name").val().trim() !== "") {
		
			// Create elements of list items to apply input values
			var listItemTitle = '<li class="list_item_title freight_sans"><p>' + $item_title.val() +'</p></li>';
			var listItemDesc = '<li class="list_item_desc freight_text"><p>' + $item_desc.val() +'</p></li>';
			
			// Create element that allows us to put all the list elements together.
			var ListItem = '<ul class="list_item clear">' + listItemTitle + listItemDesc + minusSign + markSign + removeSign +'</ul>';

			// Add the list item to the list.
			$(".list").append(ListItem);
			
			// Reset input values
			$item_title.val("");
	 		$item_desc.val("");

	 		emptyList();
	 		$enterMessage.fadeOut("fast");

	 	//If intial if parmeters arn't met the show enter item name message. 
		} else if ($("#item_name").val().trim() === "") {
			$enterMessage.fadeIn("fast");
		}
	}
});


// Remove enter item name message.
$("#item_name, #item_desc").on("input", function() {
	if ($("#item_name").val().trim().length >= 1) {
		$enterMessage.fadeOut("fast");
	}
});

// Prevent spaces intially in inputs.
$("#item_name, #item_desc").on('keypress', function(e) {
	if ($(this).val() === "" && e.which === 32) {
	   return false;
	}
});

// Add sortable list element functionality.
$(".list").sortable(function() {});

// Remove list item element.
$(".list").on("click", ".remove", function() {
	$(this).parent().toggle("slow", function() {
		$(this).remove();
		emptyList();
	});
});

// Check list item element off.
$(".list").on("click", ".mark", function(){
	$(this).siblings().addClass("mark_opacity");
	$(this).parent().appendTo(".list");
	$(this).hide();
	$(this).prev().show();
});

// Uncheck list item element.
$(".list").on("click", ".minus", function() {
	$(this).siblings().removeClass("mark_opacity");
	$(this).hide();
	$(this).next().show();
});

// Called Functions.
beginIntro();

	
